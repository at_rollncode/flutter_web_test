import 'dart:io';

import 'package:english_words/english_words.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'dart:io' show Platform;
import 'package:flutter/foundation.dart'
    show debugDefaultTargetPlatformOverride;

void main() {
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  runApp(WebTestApp());
}

class WebTestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _appName,
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: RandomWords(),
    );
  }
}

const String _appName = 'Web test app';

String get _platform {
  if (kIsWeb) return 'Web';
  if (Platform.isAndroid) return 'Android';
  if (Platform.isIOS) return 'iOS';
  return '';
}

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();
}

class RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  final _biggerFont = const TextStyle(fontSize: 18.0);
  final Set<WordPair> _saved = Set<WordPair>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('$_appName ($_platform)'),
        ),
        body: ListView.builder(itemBuilder: (BuildContext context, int i) {
          if (i.isOdd) return Divider();

          final index = i ~/ 2;
          if (index >= _suggestions.length) {
            _suggestions.addAll(generateWordPairs().take(10));
          }
          return _buildRow(_suggestions[index]);
        }));
  }

  Widget _buildRow(WordPair wordPair) {
    final bool alreadySaved = _saved.contains(wordPair);
    return ListTile(
      title: getHeroWidget(
        wordPair,
        Text(
          wordPair.asPascalCase,
          style: _biggerFont,
        ),
      ),
      trailing: getHeroWidget(
        '${wordPair}icon',
        IconButton(
          icon: Icon(
            alreadySaved ? Icons.favorite : Icons.favorite_border,
            color: alreadySaved ? Colors.red : null,
          ),
          onPressed: () {
            setState(() {
              if (alreadySaved) {
                _saved.remove(wordPair);
              } else {
                _saved.add(wordPair);
              }
            });
          },
        ),
      ),
      onTap: () async {
        bool saved = await Navigator.of(context).push(MaterialPageRoute(
                builder: (context) =>
                    _WordDetailsScreen(wordPair, alreadySaved))) ??
            alreadySaved;
        if (saved != alreadySaved) {
          setState(() {
            if (saved) {
              _saved.add(wordPair);
            } else {
              _saved.remove(wordPair);
            }
          });
        }
      },
    );
  }
}

class _WordDetailsScreen extends StatefulWidget {
  final WordPair wordPair;
  final bool isSaved;

  _WordDetailsScreen(this.wordPair, this.isSaved);

  @override
  _WordDetailsScreenState createState() => _WordDetailsScreenState();
}

class _WordDetailsScreenState extends State<_WordDetailsScreen> {
  bool _alreadySaved;

  @override
  void initState() {
    _alreadySaved = widget.isSaved;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    WordPair wordPair = widget.wordPair;
    return WillPopScope(
        child: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            title: getHeroWidget(
                wordPair,
                Text(
                  wordPair.asPascalCase,
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                )),
          ),
          body: Center(
            child: getHeroWidget(
              '${wordPair}icon',
              IconButton(
                iconSize: 60.0,
                icon: Icon(
                  _alreadySaved ? Icons.favorite : Icons.favorite_border,
                  color: _alreadySaved ? Colors.red : null,
                ),
                onPressed: () {
                  setState(() {
                    _alreadySaved = !_alreadySaved;
                  });
                },
              ),
            ),
          ),
        ),
        onWillPop: (){
          Navigator.of(context).pop(_alreadySaved);
          return Future.value(false);
        });
  }
}

Hero getHeroWidget(Object tag, Widget child) {
  return Hero(
      tag: tag,
      child: Material(
        color: Colors.transparent,
        child: child,
      ));
}
